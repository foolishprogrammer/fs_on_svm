import sqlite3 as db
from sqlite3 import Error

import os
import csv
import re
import json


def get_column_type(**kwargs):
    if kwargs.get("header") in kwargs.get("integer_header", []):
        value = int(kwargs.get("value"))
    else:
        value = float(kwargs.get("value"))
    return {
        "header": kwargs.get("header"),
        "data_type": str(type(value))[8:-2],
        "example": value,
    }


def sql_create_table(table_name, vals_list, *args):
    def _sql_integer(val):
        if val["header"] in args:
            sql = "data_{header} integer NOT_NULL,".format(**val)
        else:
            sql = "{header} integer NOT_NULL,".format(**val)
        return sql

    def _sql_string(val):
        sql = "{header} text,".format(**val)
        return sql

    def _sql_float(val):
        sql = "{header} real,".format(**val)
        return sql

    sql_data_type = {
        "int": _sql_integer,
        "str": _sql_string,
        "float": _sql_float,
    }

    column_name = []

    for line in vals_list:
        column_name.append(sql_data_type.get(line["data_type"], False)(line))

    sql_create_table = (
        "CREATE TABLE IF NOT EXISTS table_{table} (\nid integer PRIMARY KEY, \n{sql_column}"
    ).format(table=table_name, sql_column="\n".join(column_name))[:-1] + ");"
    return sql_create_table


def command(conn, query):
    try:
        curs = conn.cursor()
        curs.execute(query)
        return True
    except Error as e:
        print(e)
        return False


def migrate(conn, table_name, val_list):
    data_list = []
    data_len = 0
    for val in val_list:
        data = [val[key] for key in val]
        if data_len == 0:
            data_len = len(data)
        elif data_len != len(data):
            print("There is some data inconsistencies")
            return False
        data_list.append(data)

    many_syntax = ",".join("?" * data_len)
    query_many = "INSERT INTO table_{table} VALUES (null,{syntax})".format(
        table=table_name, syntax=many_syntax
    )

    try:
        c = conn.cursor()
        c.executemany(query_many, data_list)
        conn.commit()
    except Error as e:
        print(e)


def main(**kwargs):
    # Store config
    config = kwargs["config"]
    # Get the database name from config. "database": database name
    conn = db.connect(config.get("database", "training_data.db"))
    # Get directory where the training data stored. "training_data": path
    path = config.get("training_data", "./data")

    file_list = sorted(os.listdir(path))
    table_name = {}
    file_exception = re.compile(".ods|00")
    for file in file_list:
        if file_exception.search(file):
            continue
        with open(path + "/" + file) as csv_file:
            csv_header = csv.DictReader(csv_file, delimiter=",")
            column_name = []
            for row in csv_header:
                column_name = [
                    get_column_type(
                        header=column,
                        value=row[column],
                        # "integer_header": name of column for data type is int
                        integer_header=config.get("integer_header", ["id"]),
                    )
                    for column in row
                ]
                break

            table_name.update({file[:-4]: column_name})

    for key in table_name:
        command(
            conn,
            sql_create_table(
                key, table_name[key], *(config.get("integer_header", ["id"]))
            ),
        )

    for file in file_list:
        if file_exception.search(file):
            continue
        with open(path + "/" + file) as csv_file:
            csv_data = csv.DictReader(csv_file, delimiter=",")
            data_list = []
            for row in csv_data:
                data_list.append(row)

            migrate(conn, file[:-4], data_list)


def load_config():
    """
    Load the config file for migration to database. The file define the
    database name, training data path, and specific column to search.
    Format :
        {
                "database": database_name,
                "training_data": dir_path,
                "integer_header": [list, of, header, name,],
        }

    """
    with open("db_config.json") as file:
        config = json.load(file)

    return config


def update_config(**kwargs):
    with open("db_config.json") as file:
        config = json.load(file)
        for key in kwargs:
            config.update({key: kwargs.get(key, [False])})
        file.seek(0)
        json.dump(config, file, indent=4)
        file.truncate()


if __name__ == "__main__":
    main(config=load_config())
