import sqlite3 as db
from sqlite3 import Error
import json
from sklearn.preprocessing import StandardScaler


def command(conn, query):
    try:
        curs = conn.cursor()
        curs.execute(query)
        return True
    except Error as e:
        print(e)
        return False


def main(**kwargs):
    config = kwargs["config"]

    conn = db.connect(config.get("database", "training_data.db"))

    path = config.get("normalize_data", "./preprocessed")


def load_config():
    with open("db_config.json") as file:
        config = json.load(file)

    return config


def update_config(**kwargs):
    with open("db_config.json") as file:
        config = json.load(file)
        for key in kwargs:
            config.update({key: kwargs.get(key, [False])})
        file.seek(0)
        json.dump(config, file, indent=4)
        file.truncate()


if __name__ == "__main__":
    main(load_config())
